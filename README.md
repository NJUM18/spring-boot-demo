# SpringBoot-demo

#### 介绍

这是一个Spring Boot项目的demo


#### 项目结构
- project
    - 项目源代码
- 入门文档
    - Spring Boot入门文档

#### 开发环境
- JDK 1.8
- Maven 3.6.3
- Intellij 2020.2

#### 推荐技术栈
- 数据库
    - MySQL
- 数据库连接
    - druid数据库连接池
- 数据库操作
    - Mybatis
- 服务调用URL测试
    - Postman

#### 一些教程
- [官方文档](https://spring.io/projects/spring-boot)
- [2021版SpringBoot2零基础入门](https://www.bilibili.com/video/BV19K4y1L7MT?p=1)


